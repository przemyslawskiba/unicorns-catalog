package pl.codementors.unicorns;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.*;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
//import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.*;


/**
 * @author psysiu
 */

@RunWith(PowerMockRunner.class)
public class UnicornManagerTest {

    UnicornManager manager;
    Unicorn unicorn1, unicorn2, unicorn3, unicorn4;

    @Rule
    public ExpectedException exceptionGrabber = ExpectedException.none();

    @Before
    public void prepare() {
        manager = new UnicornManager();
        unicorn1 = new Unicorn("RainbowDash");
        unicorn2 = new Unicorn("ShiningArmor");
        unicorn3 = new Unicorn("RainbowDash");
        unicorn4 = new Unicorn("ShiningArmor");
    }

    @Test
    public void Unicorn_created_emptyCollection() {
        assertNotNull("collection should exist", manager.getUnicorns());
        assertThat("Collection should be empty", manager.getUnicorns().isEmpty(), is(true));
    }

    @Test
    public void add_paramProvided_collectionWithOneElement() {
        manager.add(unicorn1);
        assertThat(manager.getUnicorns().size(), is(1));
        assertThat(manager.getUnicorns(), hasItem(unicorn1));
    }

    @Test
    public void pair_twoDifferentUnicorns_pairedSuccessfully() throws UnicornAlreadyPairedException{
        manager.add(unicorn1);
        manager.add(unicorn2);
        manager.pair(unicorn1, unicorn2);

        assertEquals(unicorn2, manager.getPaired(unicorn1));
        assertEquals(unicorn1, manager.getPaired(unicorn2));
    }

    @Test
    public void getPaired_differentReferences_returnPairedUnicorns() throws UnicornAlreadyPairedException{
        manager.add(unicorn1);
        manager.add(unicorn2);
        manager.pair(unicorn1, unicorn2);

        assertEquals(unicorn2, manager.getPaired(unicorn3));
        assertEquals(unicorn1, manager.getPaired(unicorn4));
    }

    @Test
    public void pair_pairingNotAddedUnicorns_noPairingDone() throws UnicornAlreadyPairedException{
        manager.pair(unicorn1, unicorn2);

        assertNull(manager.getPaired(unicorn1));
        assertNull(manager.getPaired(unicorn2));
    }

    @Test
    public void pair_pairAlreadyPairedUnicorns_throwException() throws UnicornAlreadyPairedException {
        manager.add(unicorn1);
        manager.add(unicorn2);
        manager.pair(unicorn1, unicorn2);
        exceptionGrabber.expect(UnicornAlreadyPairedException.class);
        exceptionGrabber.expectMessage("At least one of the unicorns already paired.");
        manager.pair(unicorn1, unicorn2);
    }

    @Test
    public void load_FileDoesNotExist_throwException() throws UnicornException {
        File input = mock(File.class);
        when(input.exists()).thenReturn(false);

        exceptionGrabber.expect(UnicornException.class);
        exceptionGrabber.expectCause(instanceOf(FileNotFoundException.class));
        manager.load(input);
    }

    @Test
    public void load_FileIsDirectory_throwException() throws UnicornException {
        File input = mock(File.class);
        when(input.exists()).thenReturn(true);
        when(input.isDirectory()).thenReturn(true);

        exceptionGrabber.expect(UnicornException.class);
        exceptionGrabber.expectCause(instanceOf(IOException.class));
        manager.load(input);
    }

    @Test
    public void read_FileDoesNotCanRead_throwException() throws UnicornException {
        File input = mock(File.class);
        when(input.exists()).thenReturn(true);
        when(input.isDirectory()).thenReturn(false);
        when(input.canRead()).thenReturn(false);

        exceptionGrabber.expect(UnicornException.class);
        exceptionGrabber.expectCause(instanceOf(IOException.class));
        manager.load(input);
    }

    @Test
    @PrepareForTest(UnicornManager.class)

    public void read_UnicornWithoutSpace_UnicornIsLoaded() throws UnicornException, Exception {
        File input = mock(File.class);
        when(input.exists()).thenReturn(true);
        when(input.isDirectory()).thenReturn(false);
        when(input.canRead()).thenReturn(true);

        FileReader fr = mock(FileReader.class);
        BufferedReader br = mock(BufferedReader.class);
        when(br.readLine()).thenReturn("RainbowDash").thenReturn(null);


        whenNew(FileReader.class).withArguments(input).thenReturn(fr);
        whenNew(BufferedReader.class).withArguments(fr).thenReturn(br);

        manager.load(input);

        Unicorn unicorn = new Unicorn("RainbowDash");
        assertThat(manager.getUnicorns(), hasItem(unicorn));
    }

    @Test
    @PrepareForTest(UnicornManager.class)
    public void read_UnicornWithSpaces_UnicornIsLoaded() throws UnicornException, Exception {
        File input = mock(File.class);
        when(input.exists()).thenReturn(true);
        when(input.isDirectory()).thenReturn(false);
        when(input.canRead()).thenReturn(true);

        FileReader fr = mock(FileReader.class);
        BufferedReader br = mock(BufferedReader.class);
        when(br.readLine()).thenReturn("Rainbow Dash").thenReturn(null);


        whenNew(FileReader.class).withArguments(input).thenReturn(fr);
        whenNew(BufferedReader.class).withArguments(fr).thenReturn(br);

        manager.load(input);

        Unicorn unicorn = new Unicorn("Rainbow Dash");
        assertThat(manager.getUnicorns(), hasItem(unicorn));
    }

    @Test
    @PrepareForTest(UnicornManager.class)
    public void read_Unicorns_UnicornIsLoaded() throws UnicornException, Exception {
        File input = mock(File.class);
        when(input.exists()).thenReturn(true);
        when(input.isDirectory()).thenReturn(false);
        when(input.canRead()).thenReturn(true);

        FileReader fr = mock(FileReader.class);
        BufferedReader br = mock(BufferedReader.class);
        when(br.readLine()).thenReturn("RainbowDash").thenReturn("RainbowDupa").thenReturn(null);


        whenNew(FileReader.class).withArguments(input).thenReturn(fr);
        whenNew(BufferedReader.class).withArguments(fr).thenReturn(br);

        manager.load(input);

        Unicorn unicorn = new Unicorn("RainbowDash");
        Unicorn unicorn2 = new Unicorn("RainbowDupa");
        assertThat(manager.getUnicorns(), hasItem(unicorn));
        assertThat(manager.getUnicorns(), hasItem(unicorn2));
    }
}
