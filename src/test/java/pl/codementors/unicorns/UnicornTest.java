package pl.codementors.unicorns;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author psysiu
 */
public class UnicornTest {

    @Test
    public void Unicorn_providedName_nameSet() {
        String name = "rainbow";
        Unicorn unicorn = new Unicorn(name);
        assertEquals(name, unicorn.getName());
    }

    @Test
    public void Unicorn_noNameProvided_noNameSet() {
        Unicorn unicorn = new Unicorn();
        assertNull(unicorn.getName());
    }

    @Test
    public void toString_nameProvided_wellFormatedOutput() {
        Unicorn unicorn = new Unicorn("Rainbow");
        assertEquals("Unicorn(name=Rainbow)", unicorn.toString());
    }

    @Test
    public void setName_nameProvided_nameSet() {
        String name = "rainbow";
        Unicorn unicorn = new Unicorn();
        unicorn.setName(name);
        assertEquals(name, unicorn.getName());
    }

    @Test
    public void equals_differentReferencesSameFields_returnTrue() {
        Unicorn u1 = new Unicorn("AppleJack");
        Unicorn u2 = new Unicorn("AppleJack");
        assertEquals(u1, u2);

    }


}
