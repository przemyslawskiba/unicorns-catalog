package pl.codementors.unicorns;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.io.*;
import java.util.*;

/**
 * Class for storing unicorns.
 *
 * @author psysiu
 */
public class UnicornManager {

    /**
     * Set of all unicorns.
     */
    private final Set<Unicorn> unicorns = new HashSet<>();

    /**
     * Set of all unicorn pairs.
     */
    private final Set<Pair<Unicorn, Unicorn>> unicornPairs = new HashSet<>();

    /**
     * Adds new unicorn. If unicorn already exists it is ignored.
     *
     * @param unicorn
     */
    public void add(Unicorn unicorn) {
        unicorns.add(unicorn);
    }

    /**
     * @return Collections of all the unicorns. It can not be modified outside of unicorn manager.
     */
    public Collection<Unicorn> getUnicorns() {
        return Collections.unmodifiableSet(unicorns);
    }

    /**
     * Create new pair of two unicorns. If at least one of the unicorns does not exists both of them are ignored.
     *
     * @param unicorn1 First unicorn in the new pair.
     * @param unicorn2 Second unicorn in the new pair.
     * @throws UnicornAlreadyPairedException When at at least one of the provided unicorns is already paired.
     */
    public void pair(Unicorn unicorn1, Unicorn unicorn2) throws UnicornAlreadyPairedException {
        if (unicorns.contains(unicorn1) && unicorns.contains(unicorn2)) {
            if (getPaired(unicorn1) != null || getPaired(unicorn2) != null) {
                throw new UnicornAlreadyPairedException("At least one of the unicorns already paired.");
            }
            unicornPairs.add(new ImmutablePair<>(unicorn1, unicorn2));
        }
    }

    /**
     * @param unicorn One unicorn from pair.
     * @return Second unicorn from pair if pair exits.
     */
    public Unicorn getPaired(Unicorn unicorn) {
        for (Pair<Unicorn, Unicorn> pair : unicornPairs) {
            if (pair.getLeft().equals(unicorn)) {
                return pair.getRight();
            }
            if (pair.getRight().equals(unicorn)) {
                return pair.getLeft();
            }
        }
        return null;
    }

    /**
     * Loads unicorns from file. Loaded unicorns are added to the inner collection. When provided file does not
     * exist or it can not be red or is not a file the UnicornException will be thrown with appropriate IOException
     * set as a cause. File content must be formatted in a way that each line contains just a name of a unicorn.
     * Name can contain spaces.
     *
     * @param file Input file with unicorns.
     * @throws UnicornException When something with reading goes wrong.
     */
    public void load(File file) throws UnicornException {
        if (!file.exists()) {
            throw new UnicornException(new FileNotFoundException());
        }
        if (file.isDirectory() || !file.canRead()) {
            throw new UnicornException(new IOException());
        }

        try (FileReader fr = new FileReader(file);
             BufferedReader br = new BufferedReader(fr);) {

            String dupa;

            while((dupa = br.readLine()) != null) {
                Unicorn unicorn = new Unicorn(dupa);
                unicorns.add(unicorn);
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}

