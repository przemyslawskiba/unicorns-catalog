package pl.codementors.unicorns;

import lombok.*;

import java.io.Serializable;

/**
 * Unicorn class.
 *
 * @author psysiu
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Unicorn implements Serializable {

    public static final long serialVersionUID = 1;

    /**
     * Unicorn name.
     */
    private String name;

}
